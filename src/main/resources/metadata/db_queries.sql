create database assignment_tech_minfy;
use assignment_tech_minfy;

create table client_details(
    id int NOT NULL PRIMARY KEY,
    client_name varchar(255),
    client_location varchar(255)
);


create table employee_details(
    id int NOT NULL PRIMARY KEY,
    emp_name varchar(255),
    emp_pass varchar(255),
    emp_role varchar(255),
    emp_designation  varchar(255),
    employee_location varchar(255),
    client_details_id int,
	FOREIGN KEY (client_details_id) REFERENCES client_details(id)
);

INSERT INTO client_details
VALUES (101, "Samp_1", "Hyd");
INSERT INTO client_details
VALUES (102, "Samp_2", "Hyd");

INSERT INTO employee_details
VALUES (1, "emp_1","emp_1","EMPLOYEE", "s_d","hyd",101);
INSERT INTO employee_details
VALUES (2, "emp_2","emp_2","MANAGER", "s_d","hyd",101);
INSERT INTO employee_details
VALUES (3, "emp_3","emp_3","CONTRACTOR", "s_d","hyd",102);
INSERT INTO employee_details
VALUES (4, "emp_4","emp_4","DIRECTOR", "s_d","hyd",102);