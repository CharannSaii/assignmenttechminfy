package com.assignmenttechminfy.assignmenttechminfy;

import com.assignmenttechminfy.assignmenttechminfy.domain.ClientDetails;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.io.Serializable;

public class EmployeeDetailsDTO implements Serializable {

    private Integer id;
    private String empName;
    private String emp_designation;
    private String employee_location;

    public EmployeeDetailsDTO() {
    }

    public EmployeeDetailsDTO(Integer id, String empName, String emp_designation, String employee_location) {
        this.id = id;
        this.empName = empName;
        this.emp_designation = emp_designation;
        this.employee_location = employee_location;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEmpName() {
        return empName;
    }

    public void setEmpName(String empName) {
        this.empName = empName;
    }

    public String getEmp_designation() {
        return emp_designation;
    }

    public void setEmp_designation(String emp_designation) {
        this.emp_designation = emp_designation;
    }

    public String getEmployee_location() {
        return employee_location;
    }

    public void setEmployee_location(String employee_location) {
        this.employee_location = employee_location;
    }
}
