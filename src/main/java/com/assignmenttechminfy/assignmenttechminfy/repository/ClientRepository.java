package com.assignmenttechminfy.assignmenttechminfy.repository;

import com.assignmenttechminfy.assignmenttechminfy.domain.ClientDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClientRepository extends JpaRepository<ClientDetails, Integer> {
}
