package com.assignmenttechminfy.assignmenttechminfy.repository;

import com.assignmenttechminfy.assignmenttechminfy.domain.EmployeeDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EmployeeRepository extends JpaRepository<EmployeeDetails, Integer> {
    EmployeeDetails findByEmpName(String name);
}
