package com.assignmenttechminfy.assignmenttechminfy.config.jwt;

import com.assignmenttechminfy.assignmenttechminfy.EmployeeDetailsDTO;
import com.assignmenttechminfy.assignmenttechminfy.domain.EmployeeDetails;
import com.assignmenttechminfy.assignmenttechminfy.service.EmployeeService;
import com.assignmenttechminfy.assignmenttechminfy.util.JWTUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class JWTFilter extends OncePerRequestFilter {

    @Autowired
    JWTUtil jwtUtil;

    @Autowired
    EmployeeService employeeService;

    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain)
            throws ServletException, IOException {
        String authHead = httpServletRequest.getHeader("Authorization");
        String userName = null, token = null;
        if (authHead != null && authHead.startsWith("Bearer ")) {
            token = authHead.substring(7);
            userName = jwtUtil.extractEmpName(token);
        }

        if (userName != null && SecurityContextHolder.getContext().getAuthentication() == null) {
            System.out.println("----Inside validation----");
            EmployeeDetails employeeDetails = employeeService.getEmpByName(userName);
            if (jwtUtil.validateToken(token, new EmployeeDetailsDTO(employeeDetails.getId(),
                    employeeDetails.getEmpName(),
                    employeeDetails.getEmp_designation(),
                    employeeDetails.getEmployee_location()))) {
                UserDetails userDetails = User.withUsername(employeeDetails.getEmpName())
                        .password(employeeDetails.getEmp_pass())
                        //.authorities(Arrays.asList(new SimpleGrantedAuthority(employeeDetails.getEmp_role())))
                        .roles(employeeDetails.getEmp_role())
                        .build();
                UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(
                        userDetails, null, userDetails.getAuthorities()
                );
                usernamePasswordAuthenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(httpServletRequest));
                SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
            }
        }
        filterChain.doFilter(httpServletRequest,httpServletResponse);
    }
}
