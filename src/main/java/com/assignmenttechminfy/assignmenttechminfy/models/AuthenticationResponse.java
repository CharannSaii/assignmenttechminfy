package com.assignmenttechminfy.assignmenttechminfy.models;

public class AuthenticationResponse {

    public String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
