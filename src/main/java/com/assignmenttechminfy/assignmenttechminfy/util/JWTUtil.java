package com.assignmenttechminfy.assignmenttechminfy.util;

import com.assignmenttechminfy.assignmenttechminfy.EmployeeDetailsDTO;
import com.assignmenttechminfy.assignmenttechminfy.domain.EmployeeDetails;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.security.Key;
import java.util.Date;
import java.util.HashMap;
import java.util.function.Function;

@Service
public class JWTUtil {

    private Key key;

    public String extractEmpName(String token) {
        return extractClaims(token).getSubject();
    }

    public Date extractDate(String token) {
        return extractClaims(token).getExpiration();
    }

    public Boolean isTokenExpired(String token) {
        return extractDate(token).after(new Date(System.currentTimeMillis()));
    }

    private Claims extractClaims(String token) {
       return Jwts.parser().setSigningKey("Secret_Key").parseClaimsJws(token).getBody();
    }


    public String generateJwt(EmployeeDetailsDTO employeeDetails) {
        HashMap<String, Object> claims = new HashMap<>();
        claims.put("id",employeeDetails.getId());
        return createToken(claims, employeeDetails.getEmpName());
    }

    private String createToken(HashMap<String, Object> claims, String empName) {
        return Jwts.builder().setClaims(claims).setSubject(empName).setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + 1000 * 60 * 60 * 5)).signWith(SignatureAlgorithm.HS512,"Secret_Key").compact();
    }

    public Boolean validateToken(String token, EmployeeDetailsDTO employeeDetails) {
        String name  = extractEmpName(token);
        System.out.println(isTokenExpired(token));
        System.out.println(isTokenExpired(token) && name.equalsIgnoreCase(employeeDetails.getEmpName()));
         return (isTokenExpired(token) && name.equalsIgnoreCase(employeeDetails.getEmpName()));
    }
}
