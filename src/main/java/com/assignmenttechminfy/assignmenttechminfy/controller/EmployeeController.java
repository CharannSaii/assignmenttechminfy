package com.assignmenttechminfy.assignmenttechminfy.controller;

import com.assignmenttechminfy.assignmenttechminfy.EmployeeDetailsDTO;
import com.assignmenttechminfy.assignmenttechminfy.domain.ClientDetails;
import com.assignmenttechminfy.assignmenttechminfy.domain.EmployeeDetails;
import com.assignmenttechminfy.assignmenttechminfy.models.AuthenticationRequest;
import com.assignmenttechminfy.assignmenttechminfy.models.AuthenticationResponse;
import com.assignmenttechminfy.assignmenttechminfy.service.EmployeeService;
import com.assignmenttechminfy.assignmenttechminfy.util.JWTUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.util.List;

@RestController
@Transactional
public class EmployeeController {
    @Autowired
    EmployeeService employeeService;

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    JWTUtil jwtUtil;

    @GetMapping("/getEmployees")
    public List<EmployeeDetails> getEmployees() {
       return employeeService.getEmployeeList();
    }

    @GetMapping("/getEmployeeById/{id}")
    public EmployeeDetails getEmployeeById(@PathVariable("id") Integer id) {
        return employeeService.getEmployeeById(id);
    }

    @PostMapping("/createEmployee")
    public EmployeeDetailsDTO createEmployee(@RequestBody EmployeeDetails employeeDetails) {
        return employeeService.createEmployee(employeeDetails);
    }

    @PutMapping("/updateEmployeeDetails")
    public EmployeeDetails updateEmployeeDetails(@RequestBody EmployeeDetails employeeDetails) {
        return employeeService.updateEmployeeDetails(employeeDetails);
    }

    @DeleteMapping("/removeEmployee/{id}")
    public void deleteEmployees(@PathVariable("id") Integer id) {
        employeeService.deleteDetails(id);
    }

    @GetMapping("/employee")
    public String checkEmp() {
        return "<h1>Employee</h1>";
    }

    @GetMapping("/manager")
    public String checkMan() {
        return "<h1>Manager</h1>";
    }

    @GetMapping("/contractor")
    public String checkCont() {
        return "<h1>Contractor</h1>";
    }

    @GetMapping("/director")
    public String checkDire() {
        return "<h1>Director</h1>";
    }

    @PostMapping("/authenticate")
    public ResponseEntity<?> createEmployee(@RequestBody AuthenticationRequest authenticationRequest) throws Exception {
      try {
          authenticationManager.authenticate(
                  new UsernamePasswordAuthenticationToken(authenticationRequest.getUserName(), authenticationRequest.getPassword())
          );
      }catch (BadCredentialsException e){
         throw new Exception("Incorrect userName and Password", e);
      }
     EmployeeDetails employeeDetails = employeeService.getEmpByName(authenticationRequest.getUserName());
     String token = jwtUtil.generateJwt(new EmployeeDetailsDTO(
              employeeDetails.getId(),
              employeeDetails.getEmpName(),
              employeeDetails.getEmp_designation(),
              employeeDetails.getEmployee_location()));
        AuthenticationResponse authenticationResponse = new AuthenticationResponse();
        authenticationResponse.setToken(token);
        return ResponseEntity.ok(authenticationResponse);
    }
}
