package com.assignmenttechminfy.assignmenttechminfy.controller;

import com.assignmenttechminfy.assignmenttechminfy.domain.ClientDetails;
import com.assignmenttechminfy.assignmenttechminfy.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ClientController {
    @Autowired
    ClientService clientService;

    @GetMapping("/getClients")
    public List<ClientDetails> getClients() {
        return clientService.getClients();
    }

    @GetMapping("/getClientById/{id}")
    public ClientDetails getClientById(@PathVariable("id") Integer id) {
        return clientService.getClientById(id);
    }

    @PostMapping("/createClient")
    public ClientDetails createClient(@RequestBody ClientDetails clientDetails) {
        return clientService.createClient(clientDetails);
    }

    @PutMapping("/updateClientDetails")
    public ClientDetails updateClientDetails(@RequestBody ClientDetails clientDetails) {
        return clientService.updateClientDetails(clientDetails);
    }

    @DeleteMapping("/removeClient/{id}")
    public void deleteClientDetails(@PathVariable("id") Integer id) {
        clientService.deleteDetails(id);
    }
}
