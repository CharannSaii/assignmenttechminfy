package com.assignmenttechminfy.assignmenttechminfy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
public class AssignmenttechminfyApplication {

	public static void main(String[] args) {
		SpringApplication.run(AssignmenttechminfyApplication.class, args);
	}

}
