package com.assignmenttechminfy.assignmenttechminfy.service;

import com.assignmenttechminfy.assignmenttechminfy.EmployeeDetailsDTO;
import com.assignmenttechminfy.assignmenttechminfy.domain.EmployeeDetails;
import com.assignmenttechminfy.assignmenttechminfy.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class EmployeeService implements UserDetailsService {
    @Autowired
    EmployeeRepository employeeRepository;
    @Autowired
    PasswordEncoder passwordEncoder;

    public List<EmployeeDetails> getEmployeeList() {
        List<EmployeeDetails> employeeDetailsList = employeeRepository.findAll();
        createCSV(employeeDetailsList);
        return employeeDetailsList;
    }

    public EmployeeDetails getEmployeeById(Integer id) {
        return employeeRepository.findById(id).get();
    }

    public EmployeeDetails getEmpByName(String name) {
        return employeeRepository.findByEmpName(name);
    }


    public EmployeeDetailsDTO createEmployee(EmployeeDetails employeeDetails) {
        String pas = passwordEncoder.encode(employeeDetails.getEmp_pass());
        employeeDetails.setEmp_pass(pas);
        EmployeeDetails details = employeeRepository.save(employeeDetails);
        if(details != null) {
            EmployeeDetailsDTO employeeDetailsDTO = new EmployeeDetailsDTO(
                    details.getId(),
                    details.getEmpName(),
                    details.getEmp_designation(),
                    details.getEmployee_location());
            return employeeDetailsDTO;
        }
        return null;
    }

    public EmployeeDetails updateEmployeeDetails(EmployeeDetails employeeDetails) {
        return employeeRepository.save(employeeDetails);
    }

    public void deleteDetails(Integer id) {
        try {
            employeeRepository.deleteById(id);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    private void createCSV(List<EmployeeDetails> employeeDetailsList) {
        if (employeeDetailsList != null && employeeDetailsList.size() > 0) {
            PrintWriter pw = null;
            try {
                pw = new PrintWriter(new File("./src/main/resources/employeeList.csv"));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            StringBuilder builder = new StringBuilder();
            String columnNamesList = "id,emp_name,emp_designation,employee_location,client_id";
            // No need give the headers Like: id, Name on builder.append
            builder.append(columnNamesList + "\n");
            for (int i = 0; i < employeeDetailsList.size(); i++) {
                builder.append(employeeDetailsList.get(i).getId() + ",");
                builder.append(employeeDetailsList.get(i).getEmpName() + ",");
                builder.append(employeeDetailsList.get(i).getEmp_designation() + ",");
                builder.append(employeeDetailsList.get(i).getEmployee_location() + ",");
                builder.append(employeeDetailsList.get(i).getClient_details().getId() + ",");
                builder.append('\n');
            }
            pw.write(builder.toString());
            pw.close();
        }
    }

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        EmployeeDetails employeeDetails = getEmpByName(s);
        if (employeeDetails != null) {
            return User.withUsername(employeeDetails.getEmpName())
                    .password(employeeDetails.getEmp_pass())
                    //.authorities(Arrays.asList(new SimpleGrantedAuthority(employeeDetails.getEmp_role())))
                    .roles(employeeDetails.getEmp_role())
                    .build();
        } else  {
            throw new UsernameNotFoundException("Unable to retrieve user");
        }
    }
}
