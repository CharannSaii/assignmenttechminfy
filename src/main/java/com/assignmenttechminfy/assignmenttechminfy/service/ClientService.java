package com.assignmenttechminfy.assignmenttechminfy.service;

import com.assignmenttechminfy.assignmenttechminfy.domain.ClientDetails;
import com.assignmenttechminfy.assignmenttechminfy.domain.EmployeeDetails;
import com.assignmenttechminfy.assignmenttechminfy.repository.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ClientService {
    @Autowired
    ClientRepository clientRepository;

    public List<ClientDetails> getClients() {
        List<ClientDetails> clientDetails = clientRepository.findAll();
        return clientDetails;
    }

    public ClientDetails getClientById(Integer id) {
        return clientRepository.findById(id).get();
    }

    public ClientDetails createClient(ClientDetails clientDetails) {
        return clientRepository.save(clientDetails);
    }

    public ClientDetails updateClientDetails(ClientDetails clientDetails) {
        return clientRepository.save(clientDetails);
    }

    public void deleteDetails(Integer id) {
        try {
            clientRepository.deleteById(id);
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
    }
}
