package com.assignmenttechminfy.assignmenttechminfy.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "client_details")
public class ClientDetails implements Serializable {
    @Id
    private Integer id;
    private String client_name;
    private String client_location;
    @OneToMany(mappedBy="client_details")
    Set<EmployeeDetails> employeeDetails = new HashSet();

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getClient_name() {
        return client_name;
    }

    public void setClient_name(String client_name) {
        this.client_name = client_name;
    }

    public String getClient_location() {
        return client_location;
    }

    public void setClient_location(String client_location) {
        this.client_location = client_location;
    }
}

