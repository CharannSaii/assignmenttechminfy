package com.assignmenttechminfy.assignmenttechminfy.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "employee_details")
public class EmployeeDetails implements Serializable {
    @Id
    private Integer id;
    @Column(name = "emp_name")
    private String empName;
    private String emp_pass;
    private String emp_role;
    private String emp_designation;
    private String employee_location;
    @ManyToOne
    private ClientDetails client_details;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEmpName() {
        return empName;
    }

    public void setEmpName(String empName) {
        this.empName = empName;
    }

    public String getEmp_designation() {
        return emp_designation;
    }

    public void setEmp_designation(String emp_designation) {
        this.emp_designation = emp_designation;
    }

    public String getEmployee_location() {
        return employee_location;
    }

    public void setEmployee_location(String employee_location) {
        this.employee_location = employee_location;
    }

    public ClientDetails getClient_details() {
        return client_details;
    }

    public void setClient_details(ClientDetails client_details) {
        this.client_details = client_details;
    }

    public String getEmp_pass() {
        return emp_pass;
    }

    public void setEmp_pass(String emp_pass) {
        this.emp_pass = emp_pass;
    }

    public String getEmp_role() {
        return emp_role;
    }

    public void setEmp_role(String emp_role) {
        this.emp_role = emp_role;
    }

}